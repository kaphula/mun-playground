use mun_runtime::{invoke_fn, Marshal, Runtime, RuntimeBuilder, StructRef};

fn main() {
    let runtime = RuntimeBuilder::new("mun/hello/target/hello.munlib")
        .spawn()
        .expect("Failed to load munlib"); // ok

    let state = {
        let runtime_ref = runtime.borrow();
        let state: StructRef = invoke_fn!(&runtime_ref, "hello::hello").unwrap();
        let hello_val: i32 = state.get("hello").unwrap();
        println!("{}", hello_val);
        state.root(runtime.clone());
    };


}
